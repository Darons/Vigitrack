package com.arlisistem.vigitrack_1_6;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class SocialActivity extends AppCompatActivity {

    private Button facebook;
    private  Button twitter;
    private Button instagram;
    private Button send;
    private EditText name;
    private EditText mail;
    private EditText phone;
    private EditText message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        facebook = (Button)findViewById(R.id.btnFb);
        twitter = (Button) findViewById(R.id.btnTt);
        instagram = (Button) findViewById(R.id.btnIg);
        send = (Button) findViewById(R.id.button);

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSocialNetwork(Constants.FACEBOOK);
            }
        });
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSocialNetwork(Constants.TWITTER);
            }
        });
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSocialNetwork(Constants.INSTAGRAM);
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEmail();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.social,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.home) {
            this.finish();
        }

        if (id == R.id.about) {
            FragmentManager fm = getSupportFragmentManager();
            AboutDialogFragment aboutDialogFragment = AboutDialogFragment.newInstance();
            aboutDialogFragment.show(fm, null);
        }

        return super.onOptionsItemSelected(item);
    }

    public void openSocialNetwork(int app) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        String url = "";

        switch (app) {
            case Constants.INSTAGRAM:
                url = getInstagramPageURL(this);
                i.setData(Uri.parse(url));
                break;
            case Constants.FACEBOOK:
                url = getFacebookPageURL(this);
                i.setData(Uri.parse(url));
                break;
            case Constants.TWITTER:
                url = getTwitterPageURL(this);
                i.setData(Uri.parse(url));
                break;
        }
        startActivity(i);
    }

    //methods to get the right URL to use in the intent
    public String getFacebookPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + Constants.FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + Constants.FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return Constants.FACEBOOK_URL; //normal web url
        }
    }

    public String getTwitterPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            packageManager.getPackageInfo("com.twitter.android", 0);
            return "twitter://user?user_id=" + Constants.TWITTER_PAGE_ID;
        } catch (PackageManager.NameNotFoundException e) {
            return Constants.TWITTER_URL;
        }
    }

    public String getInstagramPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            packageManager.getPackageInfo("com.instagram.android", 0);
            return Constants.INSTAGRAM_PAGE_ID;
        } catch (PackageManager.NameNotFoundException e) {
            return Constants.INSTAGRAM_URL;
        }
    }

    public boolean sendEmail() {
        name = (EditText) findViewById(R.id.editName);
        mail = (EditText) findViewById(R.id.editMail);
        phone = (EditText) findViewById(R.id.editPhone);
        message = (EditText) findViewById(R.id.editMessage);

        String sender =name.getText() + "\n" + mail.getText() + "\n" + phone.getText();
        String body = Constants.BODY_MESSAGE[0] + message.getText() + Constants.BODY_MESSAGE[1] + sender;

        if(!validate()) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto:" + Constants.EMAIL));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, Constants.SUBJECT);
            emailIntent.putExtra(Intent.EXTRA_TEXT, body);
            try {
                startActivity(Intent.createChooser(emailIntent, "Enviar correo usando..."));
                return true;
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "No hay clientes de correo instalados", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Debe llenar los campos", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    public boolean validate() {
        return (name.getText().toString().equals("") || mail.getText().toString().equals("")
                || phone.getText().toString().equals("") || message.getText().toString().equals(""));
    }

}
