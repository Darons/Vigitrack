package com.arlisistem.vigitrack_1_6;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ContactActivity extends AppCompatActivity {

    private EditText phone, phtwo, phthree, pown, name, alert;
    SharedPreferences numbers;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        numbers = getSharedPreferences(Constants.PREFERENCES, MODE_PRIVATE);
        editor = numbers.edit();

        name = (EditText) findViewById(R.id.nameOwn);
        pown = (EditText) findViewById(R.id.phoneOwn);
        phone = (EditText) findViewById(R.id.phoneOne);
        phtwo = (EditText) findViewById(R.id.phoneTwo);
        phthree = (EditText) findViewById(R.id.phoneThree);
        alert = (EditText) findViewById(R.id.alertMessage);
        Button save = (Button) findViewById(R.id.save);

        if(numbers.contains(Constants.NAME_OWN))
            name.setText(numbers.getString(Constants.NAME_OWN, null));
        if(numbers.contains(Constants.PHONE_OWN))
            pown.setText(numbers.getString(Constants.PHONE_OWN, null));
        if(numbers.contains(Constants.PHONE_ONE))
            phone.setText(numbers.getString(Constants.PHONE_ONE, null));
        if(numbers.contains(Constants.PHONE_TWO))
            phtwo.setText(numbers.getString(Constants.PHONE_TWO, null));
        if(numbers.contains(Constants.PHONE_THREE))
            phthree.setText(numbers.getString(Constants.PHONE_THREE, null));
        if(numbers.contains(Constants.ALERT_MESSAGE))
            alert.setText(numbers.getString(Constants.ALERT_MESSAGE, null));

        pown.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        phtwo.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        phthree.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!name.getText().toString().equals(""))
                    editor.putString(Constants.NAME_OWN, name.getText().toString());
                else {
                    Toast.makeText(ContactActivity.this, "Debe indicar un nombre",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!pown.getText().toString().equals(""))
                    editor.putString(Constants.PHONE_OWN, pown.getText().toString().trim());
                else {
                    Toast.makeText(ContactActivity.this, "Debe indicar un número de teléfono",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!phone.getText().toString().equals(""))
                    editor.putString(Constants.PHONE_ONE, phone.getText().toString().trim());
                else if (numbers.contains(Constants.PHONE_ONE))
                    editor.remove(Constants.PHONE_ONE);

                if(!phtwo.getText().toString().equals(""))
                    editor.putString(Constants.PHONE_TWO, phtwo.getText().toString().trim());
                else if (numbers.contains(Constants.PHONE_TWO))
                    editor.remove(Constants.PHONE_TWO);

                if(!phthree.getText().toString().equals(""))
                    editor.putString(Constants.PHONE_THREE, phthree.getText().toString().trim());
                else if (numbers.contains(Constants.PHONE_THREE))
                    editor.remove(Constants.PHONE_THREE);

                if(!alert.getText().toString().equals(""))
                    editor.putString(Constants.ALERT_MESSAGE, alert.getText().toString().trim());
                else if (numbers.contains(Constants.ALERT_MESSAGE))
                    editor.remove(Constants.ALERT_MESSAGE);

                editor.apply();
                finish();
            }
        });
    }
}
