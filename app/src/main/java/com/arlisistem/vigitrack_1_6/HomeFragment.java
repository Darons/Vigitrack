package com.arlisistem.vigitrack_1_6;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment implements
        ActivityCompat.OnRequestPermissionsResultCallback, PermissionResultCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    //Objects for handling permissions
    ArrayList<String> permissions=new ArrayList<>();
    PermissionUtils permissionUtils;
    private boolean isGranted = false;

    //Data stored in preferences for sms message
    private SharedPreferences numbers;

    //Receivers for handling the sms
    private BroadcastReceiver sentReceiver, deliveredReceiver;

    //Used for handling locations
    private FusedLocationProviderClient mFusedLocationClient;
    private Location mCurrentLocation;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private boolean mRequestingLocationUpdates;
    private final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting location ley";
    private GoogleApiClient mGoogleApiClient;

    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View v = inflater.inflate(R.layout.fragment_home, container, false);

        numbers = getActivity().getSharedPreferences(Constants.PREFERENCES, MODE_PRIVATE);

        permissionUtils=new PermissionUtils(getActivity(), getActivity().getSupportFragmentManager().
            findFragmentById(R.id.frame_layout));
        permissions.add(Manifest.permission.SEND_SMS);
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        String message = "Debe permitir enviar SMS y acceder a la ubicación para usar la opción de emergencia";
        permissionUtils.check_permission(permissions,message,1);
        if (savedInstanceState != null)
            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        REQUESTING_LOCATION_UPDATES_KEY);
            }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        buildGoogleApiClient();

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    mCurrentLocation = location;
                }
            };
        };

        ImageButton emergency = (ImageButton) v.findViewById(R.id.emergency);
        emergency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cantSendMessages()) {
                    Toast.makeText(getActivity(), "El dispositivo no puede enviar mensajes", Toast.LENGTH_LONG).show();
                }
                else {
                    if(numbers.contains(Constants.PHONE_ONE) || numbers.contains(Constants.PHONE_TWO)
                            || numbers.contains(Constants.PHONE_THREE)) {
                        if (isGranted) {
                            message();
                        }
                        else {
                            permissionUtils.check_permission(permissions,"Debe permitir enviar SMS y acceder a la" +
                                    "ubicación para usar la opción de emergencia",1);
                        }
                    }
                    else {
                        Intent intent;
                        intent = new Intent(getActivity(), ContactActivity.class);
                        startActivity(intent);
                        Toast.makeText(getActivity(), "Debe agregar los números de contacto", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        return v;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    /**
     * Receivers for sent and delivered messages
     */
    @Override
    public void onResume() {
        super.onResume();

        sentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getActivity(), "Sin servicio.", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }
        };

        deliveredReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch(getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getActivity(), "Mensaje entregado con éxito.", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };
        getActivity().registerReceiver(sentReceiver, new IntentFilter("SMS_SENT"));
        getActivity().registerReceiver(deliveredReceiver, new IntentFilter("SMS_DELIVERED"));

        permissionUtils.check_permission(permissions,"Debe permitir enviar SMS y acceder a la" +
                "ubicación para usar la opción de emergencia",1);

        if (!mRequestingLocationUpdates && mGoogleApiClient.isConnected())
            startLocationUpdates();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(sentReceiver);
        getActivity().unregisterReceiver(deliveredReceiver);
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates)
            stopLocationUpdates();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle connectionHint) {

        if (mCurrentLocation == null && isGranted) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }

        if (mRequestingLocationUpdates && isGranted) {
          startLocationUpdates();
        }
    }


    /**
     * Validates if the device can send messages (SMS)
     * for example, a tablet device
     * @return
     */
    public boolean cantSendMessages() {
        PackageManager pm = getActivity().getPackageManager();
        return (!pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY) &&
                !pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_CDMA));
    }

    /**
     * Send the messages
     */
    public void message() {
        String number;
        if (numbers.contains(Constants.PHONE_ONE)) {
            number = numbers.getString(Constants.PHONE_ONE, null);
            sendMessage(number);
        }
        if (numbers.contains(Constants.PHONE_TWO)) {
            number = numbers.getString(Constants.PHONE_TWO, null);
            sendMessage(number);
        }
        if(numbers.contains(Constants.PHONE_THREE)) {
            number = numbers.getString(Constants.PHONE_THREE, null);
            sendMessage(number);
        }
        Toast.makeText(getActivity(), "Enviando mensajes...", Toast.LENGTH_SHORT).show();
    }

    public void sendMessage(String number) {
        SmsManager sms = SmsManager.getDefault();

        PendingIntent sentIntent = PendingIntent.getBroadcast(getActivity(),0,
                new Intent("SMS_SENT"), 0);
        PendingIntent deliveredIntent = PendingIntent.getBroadcast(getActivity(), 0,
                new Intent("SMS_DELIVERED"), 0);

        String name = numbers.getString(Constants.NAME_OWN, null);
        String phone = numbers.getString(Constants.PHONE_OWN, null);

        String message = name + " ( " + phone + " )";
        if (numbers.contains(Constants.ALERT_MESSAGE))
            message += " " + numbers.getString(Constants.ALERT_MESSAGE, null);
        else
            message += Constants.PANIC_MESSAGE[0];

        sms.sendTextMessage(number, null, message,
                sentIntent, deliveredIntent);

        String message2 = getLocation();
        if (!message2.equals(""))
            sms.sendTextMessage(number, null, message2,
                sentIntent, deliveredIntent);
        else
            sms.sendTextMessage(number, null, "Ubicación no disponible",
                    sentIntent, deliveredIntent);
    }

    public String getLocation() {
        if (mCurrentLocation != null) {
            double latitude = mCurrentLocation.getLatitude();
            double longitude = mCurrentLocation.getLongitude();
            return Constants.PANIC_MESSAGE[1] + String.valueOf(latitude) + "," + String.valueOf(longitude);
        }
        return "";

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // redirects to utils
        permissionUtils.onRequestPermissionsResult(requestCode,permissions,grantResults);

    }

    // Callback functions

    public void PermissionGranted(int request_code) {
        Log.i("PERMISSION","GRANTED");
        isGranted = true;
    }

    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        Log.i("PERMISSION PARTIALLY","GRANTED");
        isGranted = false;
    }

    public void PermissionDenied(int request_code) {
        Log.i("PERMISSION","DENIED");
        isGranted = false;
    }

    public void NeverAskAgain(int request_code) {
        Log.i("PERMISSION","NEVER ASK AGAIN");
        isGranted = false;
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    private void startLocationUpdates() {
        if (isGranted) {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
        }
        mRequestingLocationUpdates = true;
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        mRequestingLocationUpdates = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY,
                mRequestingLocationUpdates);
        // ...
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getActivity(), "Conexión fallida", Toast.LENGTH_SHORT);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
    }
}
