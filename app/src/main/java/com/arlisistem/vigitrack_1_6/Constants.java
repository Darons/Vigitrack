package com.arlisistem.vigitrack_1_6;

public class Constants {

    public static final String VIGITRACK = "http://www.vigitrackavl.com";
    public static final String GPSMUNDI = "http://server.gpsmundi.com/v3/m/";
    public static final String FACEBOOK_URL = "https://www.facebook.com/vigitrackavl";
    public static final String FACEBOOK_PAGE_ID = "360034857376113";
    public static final int FACEBOOK = 2;
    public static final int TWITTER = 3;
    public static final int INSTAGRAM = 1;
    public static final String TWITTER_PAGE_ID = "534586050";
    public static final String TWITTER_URL = "https://twitter.com/vigitrack_1_6";
    public static final String INSTAGRAM_URL = "https://www.instagram.com/vigitrack_1_6";
    public static final String INSTAGRAM_PAGE_ID = "http://instagram.com/_u/vigitrack_1_6";
    public static final String EMAIL = "franquicia@vigitrackavl.com";
    public static final String[] BODY_MESSAGE = {"Estimados Vigitrack AVL.\n\n", "\n\nAtentamente:\n\n"};
    public static final String SUBJECT = "Feedback";
    public static final String PREFERENCES = "phone_numbers";
    public static final String NAME_OWN = "name";
    public static final String PHONE_OWN = "own";
    public static final String PHONE_ONE = "one";
    public static final String PHONE_TWO = "two";
    public static final String PHONE_THREE = "three";
    public static final String[] PANIC_MESSAGE ={" se encuentra en peligro. Contactar urgentemente.",
            " Ubicación: https://maps.google.com/?q="};
    public static final String ALERT_MESSAGE = "alert";
}